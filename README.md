This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Accounts Receivable App

A totally, 100% realistic and non-contrived application for managing invoices.

The following tools and libraries are used in the project:

1. React v17
1. Redux for state managment
1. React Router for client-side routing
1. Jest as the test runner
1. React Testing Library for executing React Components
1. ESLint for linting
1. Prettier for auto-formatting

## Project Todos

To be conscientious of your valuable time, please limit yourself to 3
hours. This time limit does not need to be consecutive. We have no expectations
of when you would be able to work on this project, so over the course of a
weekend is just fine.

Please perform your work as though you were delivering it to a paying client and
deploying it into production. We care more about the quality of your solution
than necessarily how far you get.

Complete the following tasks as explained in their linked issues:

1. Compute Total: https://gitlab.com/testdouble/accounts-receivable-react/-/issues/7
2. Compute Invoice Discounts: https://gitlab.com/testdouble/accounts-receivable-react/-/issues/2
2. Add "Invoice Complete" radio button: https://gitlab.com/testdouble/accounts-receivable-react/-/issues/4
3. Incomplete Invoice count: https://gitlab.com/testdouble/accounts-receivable-react/-/issues/5
4. Change Invoice state: https://gitlab.com/testdouble/accounts-receivable-react/-/issues/6

NOTE: If you receive alternative instructions from a Test Double Agent for this
project, then stick to what they said.

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.<br>
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.
